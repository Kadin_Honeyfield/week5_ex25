using System;

namespace week5_ex25
{
    partial class Program
    {
        private class Checkout
        {
            int quantity;
            double price;
            public Checkout(int _quantity, double _price)
            {
                quantity = _quantity;
                price = _price;
            }

            public void Calculate()
            {
                Console.WriteLine($"Total of items {quantity+price}");
            }
        }
       
    }
    
}
