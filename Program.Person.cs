using System;

namespace week5_ex25
{
    partial class Program
    {
        private class Person
        {
            string Name;
            int Age;
            public Person(string _name, int _age)
            {
                Name = _name;
                Age = _age;
            }
            public void SayHello()
            {
                Console.WriteLine($"My name is {Name} and I am {Age}");
            }
        }
       
    }
    
}
