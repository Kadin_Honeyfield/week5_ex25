﻿namespace week5_ex25
{
    partial class Program
    {
        static void Main(string[] args)
        {

            /* Exercise A *//*
            var sam = new Person("Sam", 12);
            sam.SayHello();*/

            /* Exercise B */
            var a1 = new Checkout(3, 4.50);
            var a2 = new Checkout(6, 7.99);
            var a3 = new Checkout(2, 0.39);

            a1.Calculate();
            a3.Calculate();
            a2.Calculate();
            
        }
       
    }
    
}
